# Security Releases (Critical / Non-critical) as a Developer

The release deadlines for a critical or non-critical security release are different.
Check the [Release deadlines](process.md) first to know when the security
fixes have to be merged by.

## DO NOT PUSH TO `gitlab-org/gitlab`

As a developer working on a fix for a security vulnerability, your main concern
is not disclosing the vulnerability or the fix before we're ready to publicly
disclose it.

To that end, you'll need to be sure that security vulnerabilities:

* For GitLab.com, are fixed in the [GitLab Security Repo].
* For Omnibus, are fixed in the [Omnibus Dev project].

This is fundamental to our security release process because both repositories are not publicly-accessible.

## Process

As with most GitLab development, a security fix starts with an issue identifying
the vulnerability. In this case, it should be a confidential issue on the `gitlab-org/gitlab`
project on [GitLab.com]

Once a security issue is assigned to a developer, we follow the same merge
request and code review process as any other change, but on [GitLab Security Repo].
All security fixes are released for [at least three monthly releases], and you
will be responsible for creating backports as well.

When working on a high severity ~S1 issue, the initial work on a [post-deployment patch](#creating-a-post-deployment-patch) as described below, may be done before any
other tasks in the normal workflow at the request of the security engineer, with
the review of the delivery team and infrastructure team as outlined in the
post-deployment patch process.

### Preparation

Before starting, add the new `security` remote on your local GitLab repository:

```
git remote add security git@gitlab.com:gitlab-org/security/gitlab.git
```

From %12.6, the development of security fixes was moved to [a private group on GitLab.com](https://gitlab.com/groups/gitlab-com/gl-infra/-/epics/121),
make sure you remove any `dev.gitlab.org` remotes from your local repository:

```
git remote -v | grep dev.gitlab.org | awk '{print $1}' | uniq | xargs git remote rm
```

Finally, run the `scripts/security-harness` script. This script will install a Git `pre-push` hook
that will prevent pushing to any remote besides `gitlab.com/gitlab-org/security` or `dev.gitlab.org`,
in order to prevent accidental disclosure.

Please make sure the output of running `scripts/security-harness` is:

```
Security harness installed -- you will only be able to push to dev.gitlab.org or gitlab.com/gitlab-org/security
```

### Branches

Because all security fixes go into [at least three monthly releases] (which
includes the current/next one), you'll be creating at least three branches for
your fix. Merge requests for previously released versions should target the
appropriate stable branches. For example, a fix for version 11.2.3 should
target the branch `11-2-stable`. In the past we used to target dedicated
security branches, but this is no longer the case.

Your branch name must start with `security`, such as: `security-rs-milestone-xss-10-6`.

Branch names that start with `security` will be rejected upon a push from the [GitLab] and [GitLab-FOSS]
repositories on GitLab.com. This helps ensure security merge requests don't get
leaked prematurely.

### Development

#### Create an issue

Create an issue on the respective repo, [GitLab Security Repo] or [Omnibus Dev Project], using the
[Security Developer Workflow]. The title should be the same as the original created on [GitLab.com],
for example: `Prevent stored XSS in code blocks`.

This issue is now your "Implementation issue" and a single source of truth for
all related issues and merge requests. Once the issue is created, assign it
to yourself and start working on the tasks listed there.

#### Create merge requests

The above issue will ask you to create merge requests on [GitLab Security Repo] or [Omnibus Dev Project].
Ensure they are using [Security Release template].

For every merge request created, make sure:

* It's referenced on the "Links" section on your implementation issue.
* Targets the `X-Y-stable{,-ee}` branch that belongs to the target version. 
* Has a milestone assigned, e.g. a `11-10-stable-` backport MR would have `11.10` set as its milestone.
  * Milestones that have been closed are not displayed in the UI, but can still be set using the quick action command `/milestone %X.Y`
* Has correct labels (normally ~security is enough)
* Has green pipelines.

**IMPORTANT:** 
* In case one of these items is missing or incorrect, Release Managers will re-assign
all related merge requests to the original author and remove the issue from the current security release.
This is necessary due to the number of merge requests that need to be handled for each security release. 
* Merge requests targeting master are to be merged by Release Managers only.

When creating your merge requests backports there's a handy script, [`secpick`](#secpick-script),
that will allow you to cherry-pick commits across multiple releases.

### Final steps

Once your MR is assigned to the `@gitlab-release-tools-bot` before the changes
are merged, you should update the Security Release issue description right away
with links to your implementation issue and MRs. This issue can be found in the
topic of the #releases channel, and the issue title should specify the same
three versions (XX.YY) that your MR has backports for. If these versions don't
match up, contact your maintainer for the security issue, a release manager, or
the #releases channel right away.

After the Release Manager promotes the packages to public, your fix will need to go into into the `master`
branch to ensure that it's included in all future releases. The release manager will merge
the MR, so make sure all the MRs, including the MR targeting the master branch have the
`security` label.

Be sure to run `scripts/security-harness` again to enable pushing to remotes
other than [GitLab Security] or `dev.gitlab.org`!

### Questions?

If you have any doubts or questions, feel free to ask for help in the #development
channel in Slack.

#### Creating a post-deployment patch

For high severity ~S1 issues, a post-deployment patch may be required.

If a post-deployment patch was not already created as part of the initial response
to an ~S1 issue, verify with the security engineer if one is required. If an
existing patch exists, work with the security engineer to determine if the
temporary patch should be replaced with one based on the completed solution.

Follow the [post-deployment patch process](../deploy/post-deployment-patches.md).

#### `secpick` script

This is a small script that helps cherry-picking across multiple releases. It will stop
if there is a conflict while cherry-picking, otherwise will push the change to `org`.

The list of options available running:

```
$ bin/secpick --help
```

For example:

```
bin/secpick -v 10.6 -b security-fix-mr-issue -s SHA
```

You can also pick a range of commits. To pick a range from `aaaa` (inclusive)
to `bbbb` you can:


```
bin/secpick -v 10.6 -b security-fix-mr-issue -s aaaa^..bbbb
```

It will change local branches to push to a new security branch for each specified release,
meaning that local changes should be saved prior to running the script.

---

[Return to Security Guide](process.md)

[at least three monthly releases]: https://docs.gitlab.com/ee/policy/maintenance.html#security-releases
[GitLab.com]: https://gitlab.com/
[GitLab Security]: https://gitlab.com/gitlab-org/security/ 
[GitLab Security repo]: https://gitlab.com/gitlab-org/security/gitlab
[Omnibus Dev project]: https://dev.gitlab.org/gitlab/omnibus-gitlab
[Security Developer Workflow]: https://gitlab.com/gitlab-org/security/gitlab/issues/new?issuable_template=Security+developer+workflow
[scripts/security-harness]: https://gitlab.com/gitlab-org/gitlab/blob/master/scripts/security-harness
[GitLab]: https://gitlab.com/gitlab-org/gitlab
[GitLab-FOSS]: https://gitlab.com/gitlab-org/gitlab-foss
[Security Release template]: https://gitlab.com/gitlab-org/gitlab/blob/master/.gitlab/merge_request_templates/Security%20Release.md
