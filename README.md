# GitLab Release Process

This repository contains instructions for releasing new versions of
GitLab Community Edition (CE), Enterprise Edition (EE) and GitLab.com release
related processes.

The goal is to provide clear instructions and procedures for our entire release
process. This repository includes documentation which should help perform the
role of [Release Manager](#release-manager) as well as
documentation that should help other stake-holders in the release process.

The topics are divided per each type of release. Each type of release has a
general process overview and specific documentation for different stakeholders.

## Release Manager

- [Getting started](release_manager/index.md#getting-started)
- [Offboarding](release_manager/index.md#offboarding)

## Releasing monthly stable version

- [General process overview](general/monthly/process.md)
- [Release manager in monthly release](general/monthly/release-manager.md)

## Releasing patch versions

- [General process overview](general/patch/process.md)

## Security release

- [General process overview](general/security/process.md)
- [Release manager in security release](general/security/release-manager.md)
- [Security engineer in security release](general/security/security-engineer.md)
- [Developer in security release](general/security/developer.md)
- [Quality engineer in security release](general/security/quality.md)

## Building

- [Overview of Building Packages](general/building-packages.md)

## Deployment

- [Traffic Generation](general/deploy/traffic-generation.md)
- [Post Deployment Patches](general/deploy/post-deployment-patches.md)
- [Auto-deploy process](general/deploy/auto-deploy.md)
- [Deploy Failures](general/deploy/failures.md)
- [Temporarily stopping auto-deploy process](general/deploy/stopping-auto-deploy.md)

## Guides

- [Overview of our Tooling](general/tooling.md)
- [How to release new minor versions of GitLab each month](general/monthly.md)
- [How to release patch versions of GitLab](general/patch/process.md)
- [How to create release candidates for new monthly versions of GitLab](general/release-candidates.md)
- [How to remove packages from packages.gitlab.com](general/remove-packages.md)
- [Required permissions to tag and deploy a release](general/permissions.md)
- [Guidelines for a new major release of GitLab](general/major.md)
- [Pro tips](general/pro-tips.md)
- [Release template files](https://gitlab.com/gitlab-org/release-tools/tree/master/templates)
- [How to create a blog post for a patch release](general/patch/blog-post.md)

## Further Reading

- ["Release Manager - The invisible hero"](https://about.gitlab.com/2015/06/25/release-manager-the-invisible-hero/) (2015-06-25)
- ["How we managed 49 monthly releases"](https://about.gitlab.com/2015/12/17/gitlab-release-process/) (2015-12-17)

## Outdated documentation

The documentation below is outdated and only remains linked to prevent dead links

- [Exception requests](general/exception-request/process.md)
- [CE to EE merge](general/merge-ce-into-ee.md)
- [Manual QA testing for Release Managers](general/qa-checklist.md)
- [How to pick changes for Omnibus GitLab](general/pick-to-omnibus-gitlab.md)
- [How to push a new omnibus tag version](general/omnibus-tag.md)
- [How to pick specific changes into `stable` branches](general/pick-changes-into-stable.md)
- [How to push to multiple remotes at once](general/push-to-multiple-remotes.md)

## Contributing

See [CONTRIBUTING.md](./CONTRIBUTING.md).

## License

See [LICENSE](./LICENSE).
